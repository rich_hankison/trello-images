#Hi Dick!  
#Let's get started...

## Trello Images Project

**Local setup**

2.  clone project - run `git clone https://bitbucket.org/rich_hankison/trello-images.git`
3.  `cd` into your new directory
4.  Run `composer install` from your terminal
5.  Run `php artisan key:generate` from your terminal to generate your [CSRF key](https://laravel.com/docs/5.8/csrf)
6.  Create .env file by removing `.example` from your existing .env.example file
7.  Run `php artisan serve` to boot up your local server

**Trello set up**

2.  [Navigate here](https://trello.com/app-key/) and find your API key, and generate your [API token](https://d.pr/free/i/qcAR1i)
3.  Add your API key and token to [your .env file.](https://d.pr/free/i/yjhnmo)
4.  [See board](https://trello.com/b/j816InXN/trello-images-board)
5.  [See API pull of first card](/test)
6.  Check out code for how the pull: `routes/web.php @/test`
7.  [Ref Trello API package documents](https://github.com/cdaguerre/php-trello-api/blob/master/docs/Api/Index.md)
8.  Get crackin!
