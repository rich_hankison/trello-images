<script src="https://p.trellocdn.com/power-up.js"></script>

<script>
	let t = TrelloPowerUp.initialize({
		'card-buttons': function(t, options){
			// get card data and store it to variables
			return [{
				// set details of button icon
				icon: 'https://cdn.glitch.com/1b42d7fe-bda8-4af8-a6c8-eff0cea9e08a%2Frocket-ship.png?1494946700421',
				text: 'Add Picture',
				callback: function(t){
					t.card('all')
					.then(function (card) {
						let name = card.name; // "hawk goes to work"
						let id = card.id; // "5d5435a70d9db3681093f37b"
						console.log(name, id);
						console.log(JSON.stringify({name: name, id: id}));
						// ping internal server to this app to grab key phrase, get image search, add attachment
						fetch('https://11a59900.ngrok.io/api/add-image',{
							method: 'post',
							headers: { 
								'Content-Type': 'application/json'
							},
							body: JSON.stringify({name: name, id: id})
						})
						.then(function(response) {
							return response.text();
						}).then(function(data) {
							console.log(data);
						});
					});					
				}
			}];
		}
	});
</script>
