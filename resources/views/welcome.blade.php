<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Trello Images</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: left;
            font-size: 18px;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}">Home</a>
            @else
            <a href="{{ route('login') }}">Login</a>

            @if (Route::has('register'))
            <a href="{{ route('register') }}">Register</a>
            @endif
            @endauth
        </div>
        @endif

        <div class="content">
            <div class="title m-b-md">
                Hi Dick! </br>Let's get started...
            </div>
            <h2> Trello Images Project </h2>
            <ol>
                <b>Local setup</b>
                <li>clone project - run <code>git clone https://bitbucket.org/rich_hankison/trello-images.git</code></li>
                <li><code>cd</code> into your new directory</li>
                <li>Run <code>composer install </code> from your terminal</li>
                <li>Run <code>php artisan key:generate </code> from your terminal</li>
                <li>Create .env file by removing <code>.example</code> from your existin .env.example file</li>
            </ol>
            <ol>
                <b>Trello set up</b>
                <li><a href="https://trello.com/app-key/">Navigate here</a> and find your API key, and generate your <a href="https://d.pr/free/i/qcAR1i">API token</a></li>
                <li>Add your API key and token to <a href="https://d.pr/free/i/yjhnmo">your .env file.</a> </li>
                <li><a href="https://trello.com/b/j816InXN/trello-images-board">See board</a></li>
                <li><a href="/test">See API pull of first card</a></li>
                <li>Check out code for how the pull: <code>routes/web.php @/test</code></li>
                <li><a href="https://github.com/cdaguerre/php-trello-api/blob/master/docs/Api/Index.md">Ref Trello API package documents</a></li>
                <li>Get crackin!</li>
            </ol>
        </div>
    </div>
</body>
</html>
