<?php

namespace App\Providers;

use App\Letters\Bing\BingSearchLetter;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class BingSearchServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('bing', function ($app) {
            return new BingSearchLetter;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Client $client)
    {
    }
}
