<?php

namespace App\Letters\Bing;

use GuzzleHttp\Client;
use Illuminate\Support\Collection;

class BingSearchLetter
{
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://api.cognitive.microsoft.com/bing/v7.0/',
            'headers' => [
                'Ocp-Apim-Subscription-Key' => '259a3e83e9a14ba6b5220e46d1fa2f4b'
            ],
        ]);
    }

    public function imageSearch(string $query) : self
    {
        // dd($this->client);
        $this->promise = $this->client->getAsync('images/search', [
            'query' => [
                'q' => $query,
            ]
        ]);
        return $this;
    }

    public function dispatch()
    {
        return collect(json_decode($this->promise->wait()->getBody()->getContents())->value);
    }

    public function keyPhraseSearch(string $query)
    {
        $client = new Client([
            'base_uri' => 'https://westcentralus.api.cognitive.microsoft.com/text/analytics/v2.1/keyPhrases',
            'headers' => [
                'Ocp-Apim-Subscription-Key' => '7b14e8104aed476d847ec1316ae538d1'
            ],
        ]);
        
        try {
            $response = $client->post('', [
                'json' => [
                    'documents' => [
                        [
                            'id' => 1,
                            'text' => $query,
                            'language' => 'en'
                        ]
                    ]
                ]
            ]);
            return json_decode($response->getBody()->getContents())->documents[0]->keyPhrases[0];
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
