<?php

use App\Letters\Bing\BingSearchLetter;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('test', function () {
    return 'test';
});

Route::any('/add-image', function (Request $request) {
    $card_id = $request->id;
    $card_name = $request->name;

    $key_phrase = Bing::keyPhraseSearch($card_name);
    $image_url = Bing::imageSearch($key_phrase)->dispatch()->random()->thumbnailUrl;

    return Trello::cards()->attachments()->create(
        $card_id,
        [
            'name' => $card_name,
            'url' => $image_url,
        ]
    );

    return $image_url;
});

Route::any('trello/webhook', function (Request $request) {
    $action = $request->action;

    logger($request->all());

    if ($action['type'] == 'createCard' || $action['type'] == 'updateCard') {
        $card = $action['data']['card'];

        $key_phrase = $card['name']; // (new BingSearchLetter)->entityTextSearch($card['name']);
        $key_phrase = (new BingSearchLetter)->entityTextSearch($card['name']);
        $image_url = (new BingSearchLetter)->imageSearch($key_phrase)->dispatch()->random()->contentUrl;
        
        Trello::cards()->attachments()->create(
            $card['id'],
            [
                'name' => $key_phrase,
                'url' => $image_url,
            ]
        );
    }
});
