<?php

use App\Letters\Bing\BingSearchLetter;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/trello-powerup', function () {
    return view('trello-powerup');
});

// documentation: https://github.com/cdaguerre/php-trello-api/blob/master/docs/Api/Index.md
Route::get('test', function () {
    phpinfo();
    $card = Trello::manager()->getCard('eJC5BGPB');

    $keyPhraseClient = new Client();
    $keyResponse = $keyPhraseClient->post('https://apis.paralleldots.com/v4/phrase_extractor', [
    'form_params' => [
      'api_key' => 'VS7aQZRHoIHQqBKTOWtYfyMHEHmtuGDInGSiuVH9GdA',
      'text' => $card->getName()
    ]
  ]);
    $phrases = json_decode($keyResponse->getBody()->getContents())->keywords;
    $phrase = collect($phrases)->count() ? collect($phrases)->random()->keyword : $card->getName();

    $client = new Client();
    $response = $client->get('https://pixabay.com/api/?key=13869432-42119f84e217813b70c72be81&q='.$phrase);
    $images = json_decode($response->getBody()->getContents())->hits;
    $image = collect($images)->random();
    // dd($card->getId());
    dd(Trello::cards()->attachments()->create(
        $card->getId(),
        [
      'name' => $card->getName(),
      'url' => $image->previewURL,
    ]
    ));

    dd($card['name'], $card);
});

Route::get('update-cover', function (Request $request) {
    $cardId = explode('/', explode('https://trello.com/c/', $request->url)[1])[0];
    $card = Trello::manager()->getCard($cardId);

    // get key phrase
    $keyPhraseClient = new Client();
    $keyResponse = $keyPhraseClient->post('https://apis.paralleldots.com/v4/phrase_extractor', [
    'form_params' => [
      'api_key' => 'VS7aQZRHoIHQqBKTOWtYfyMHEHmtuGDInGSiuVH9GdA',
      'text' => $card->getName()
    ]
  ]);
    $phrases = json_decode($keyResponse->getBody()->getContents())->keywords;
    $phrase = collect($phrases)->count() ? collect($phrases)->random()->keyword : $card->getName();

    // get image
    $client = new Client();
    $response = $client->get('https://pixabay.com/api/?key=13869432-42119f84e217813b70c72be81&q='.$phrase);
    $images = json_decode($response->getBody()->getContents())->hits;
    $image = collect($images)->count() ? collect($images)->random() : null;

    if ($image) {
        // upload image to card
        Trello::cards()->attachments()->create(
            $card->getId(),
            [
        'name' => $phrase,
        'url' => $image->previewURL,
      ]
        );
    } else {
        Trello::cards()->attachments()->create(
            $card->getId(),
            [
        'name' => 'No image found',
        'url' => 'https://picsum.photos/200/300?grayscale',
      ]
        );
    }
    return '<script>window.close()</script>';
});

Route::get('query', function (Request $request) {
    return dd((new BingSearchLetter)->imageSearch('kitty')->dispatch()->value[0]->contentUrl);
});

Route::get('webhook', function () {
    Trello::webhooks()->remove('5d54bd155217213c97cb25a7');
    dd(Trello::token()->webhooks()->all(env('TRELLO_API_TOKEN')));

    $webhook = Trello::webhooks()->create([
    'description' => 'new card created',
    'callbackURL' => 'https://2b3a2dde.ngrok.io/api/trello/webhook',
    'idModel' => '5d4f31226a3c6b4342420138',
    'active' => true
  ]);
    dd($webhook);
});

Route::get('entities', function (Request $request) {
    $key_phrase = "Update tonya's macbook so Claudia can use it";
    return (new BingSearchLetter)->entityTextSearch($key_phrase);
});

$webhook = array(
  'model' =>
  array(
    'id' => '5d4f31226a3c6b4342420138',
    'name' => 'Trello Images Board',
    'desc' => null,
    'descData' => null,
    'closed' => false,
    'idOrganization' => '51632492bf79c19f1c008699',
    'pinned' => false,
    'url' => 'https://trello.com/b/j816InXN/trello-images-board',
    'shortUrl' => 'https://trello.com/b/j816InXN',
    'prefs' =>
    array(
      'permissionLevel' => 'org',
      'hideVotes' => false,
      'voting' => 'disabled',
      'comments' => 'members',
      'invitations' => 'members',
      'selfJoin' => true,
      'cardCovers' => true,
      'isTemplate' => false,
      'cardAging' => 'regular',
      'calendarFeedEnabled' => false,
      'background' => '5d4c466eb39190543ce2d84d',
      'backgroundImage' => 'https://trello-backgrounds.s3.amazonaws.com/SharedBackground/1365x2048/70cd6059e980e33fb0b64c16691687fa/photo-1565207470635-d14c3e9152db',
      'backgroundImageScaled' =>
      array(
        0 =>
        array(
          'width' => 67,
          'height' => 100,
          'url' => 'https://trello-backgrounds.s3.amazonaws.com/SharedBackground/67x100/39b36e24827af4237bcbfd1a45e84308/photo-1565207470635-d14c3e9152db.jpg',
        ),
        1 =>
        array(
          'width' => 128,
          'height' => 192,
          'url' => 'https://trello-backgrounds.s3.amazonaws.com/SharedBackground/128x192/39b36e24827af4237bcbfd1a45e84308/photo-1565207470635-d14c3e9152db.jpg',
        ),
        2 =>
        array(
          'width' => 320,
          'height' => 480,
          'url' => 'https://trello-backgrounds.s3.amazonaws.com/SharedBackground/320x480/39b36e24827af4237bcbfd1a45e84308/photo-1565207470635-d14c3e9152db.jpg',
        ),
        3 =>
        array(
          'width' => 640,
          'height' => 960,
          'url' => 'https://trello-backgrounds.s3.amazonaws.com/SharedBackground/640x960/39b36e24827af4237bcbfd1a45e84308/photo-1565207470635-d14c3e9152db.jpg',
        ),
        4 =>
        array(
          'width' => 683,
          'height' => 1024,
          'url' => 'https://trello-backgrounds.s3.amazonaws.com/SharedBackground/683x1024/39b36e24827af4237bcbfd1a45e84308/photo-1565207470635-d14c3e9152db.jpg',
        ),
        5 =>
        array(
          'width' => 853,
          'height' => 1280,
          'url' => 'https://trello-backgrounds.s3.amazonaws.com/SharedBackground/853x1280/39b36e24827af4237bcbfd1a45e84308/photo-1565207470635-d14c3e9152db.jpg',
        ),
        6 =>
        array(
          'width' => 1280,
          'height' => 1920,
          'url' => 'https://trello-backgrounds.s3.amazonaws.com/SharedBackground/1280x1920/39b36e24827af4237bcbfd1a45e84308/photo-1565207470635-d14c3e9152db.jpg',
        ),
        7 =>
        array(
          'width' => 1066,
          'height' => 1600,
          'url' => 'https://trello-backgrounds.s3.amazonaws.com/SharedBackground/1066x1600/39b36e24827af4237bcbfd1a45e84308/photo-1565207470635-d14c3e9152db.jpg',
        ),
        8 =>
        array(
          'width' => 1365,
          'height' => 2048,
          'url' => 'https://trello-backgrounds.s3.amazonaws.com/SharedBackground/1365x2048/70cd6059e980e33fb0b64c16691687fa/photo-1565207470635-d14c3e9152db',
        ),
      ),
      'backgroundTile' => false,
      'backgroundBrightness' => 'dark',
      'backgroundBottomColor' => '#4b3e34',
      'backgroundTopColor' => '#cdc5c5',
      'canBePublic' => true,
      'canBeEnterprise' => true,
      'canBeOrg' => true,
      'canBePrivate' => true,
      'canInvite' => true,
    ),
    'labelNames' =>
    array(
      'green' => null,
      'yellow' => null,
      'orange' => null,
      'red' => null,
      'purple' => null,
      'blue' => null,
      'sky' => null,
      'lime' => null,
      'pink' => null,
      'black' => null,
    ),
  ),
  'action' =>
  array(
    'id' => '5d53a1c6ce46a33eca1ee342',
    'idMemberCreator' => '5142abf553845ab661001957',
    'data' =>
    array(
      'card' =>
      array(
        'id' => '5d53a1c6ce46a33eca1ee341',
        'name' => 'new trello card',
        'idShort' => 4,
        'shortLink' => 'SMQ7dfdf',
      ),
      'list' =>
      array(
        'id' => '5d501998ae661c674c65a1ad',
        'name' => 'First list',
      ),
      'board' =>
      array(
        'id' => '5d4f31226a3c6b4342420138',
        'name' => 'Trello Images Board',
        'shortLink' => 'j816InXN',
      ),
    ),
    'type' => 'createCard',
    'date' => '2019-08-14T05:53:10.049Z',
    'limits' =>
    array(
    ),
    'display' =>
    array(
      'translationKey' => 'action_create_card',
      'entities' =>
      array(
        'card' =>
        array(
          'type' => 'card',
          'id' => '5d53a1c6ce46a33eca1ee341',
          'shortLink' => 'SMQ7dfdf',
          'text' => 'new trello card',
        ),
        'list' =>
        array(
          'type' => 'list',
          'id' => '5d501998ae661c674c65a1ad',
          'text' => 'First list',
        ),
        'memberCreator' =>
        array(
          'type' => 'member',
          'id' => '5142abf553845ab661001957',
          'username' => 'richardhankison',
          'text' => 'Richard Hankison',
        ),
      ),
    ),
    'memberCreator' =>
    array(
      'id' => '5142abf553845ab661001957',
      'avatarHash' => 'dc9e9d3301df4330a371418a47ccdb4c',
      'avatarUrl' => 'https://trello-avatars.s3.amazonaws.com/dc9e9d3301df4330a371418a47ccdb4c',
      'fullName' => 'Richard Hankison',
      'idMemberReferrer' => null,
      'initials' => 'RH',
      'nonPublic' =>
      array(
      ),
      'nonPublicAvailable' => false,
      'username' => 'richardhankison',
    ),
  ),
);

Route::get('inspect-webhook', function () use ($webhook) {
    dd($webhook);
});
